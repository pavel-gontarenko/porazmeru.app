<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Защищенные свойства модели.
     * @var array
     */
    protected $guarded = [];
}
