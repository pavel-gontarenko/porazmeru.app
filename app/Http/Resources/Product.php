<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Product extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'title_k' => $this->title_k,
            'code' => $this->code,
            'code_k' => $this->code_k,
            'price' => $this->price,
            'price_o' => $this->price_o,
            'price_k' => $this->price_k,
            'description' => $this->description,
            'images' => $this->images,
            'link' => $this->link,
            'tag_id' => $this->tag_id,
            'verified' => $this->verified,
            'category_code' => $this->category_code,
            'category' => ($this->category_code) ? $this->category->name : '',
            'created_at' => ($this->category_code) ? $this->created_at->format('d.m.Y h:i:s') : '',
            'updated_at' => ($this->category_code) ? $this->updated_at->format('d.m.Y h:i:s') : '',
        ];
    }
}
