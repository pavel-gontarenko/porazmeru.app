<?php

namespace App\Http\Controllers;

use App\Http\Resources\TagResource;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    /**
     * Получение списка всех тегов.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function all()
    {
        return TagResource::collection(Tag::orderBy('id', 'asc')->get());
    }

    /**
     * Обновление данных кокретного сниппета.
     */
    public function update()
    {
        Tag::find(request()->id)->update([
            'list' => json_encode(request()->list),
            'category_code' => request()->category_code,
        ]);
    }

    /**
     * Добавление в базу нового сниппета.
     * @return mixed
     */
    public function store()
    {
        return Tag::create(
            [
                'list' => json_encode(request()->list),
                'category_code' => request()->category_code
            ]
        );
    }

    /**
     * Удаление из базы конкретного сниппета.
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function destroy($id)
    {
        Tag::find($id)->delete();

        return $this->all();
    }
}
