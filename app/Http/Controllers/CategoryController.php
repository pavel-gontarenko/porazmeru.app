<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCategory;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreCategory $request
     * @return Category
     */
    public function store(StoreCategory $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->code = $request->code;
        $category->save();
        return $category;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param StoreCategory $request
     * @param Category $category
     * @return mixed
     */
    public function update(StoreCategory $request, Category $category)
    {
        return Category::where('id', $request->id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        Category::where('id', $category->id)->delete();
    }

    /**
     * Получение списка всех категорий.
     * @return mixed
     */
    public function all()
    {
        return Category::orderBy('name', 'asc')->get();
    }

    /**
     * Подсчет количества категорий для главной статистики.
     * @return int
     */
    public function count()
    {
        return Category::all()->count();
    }
}
