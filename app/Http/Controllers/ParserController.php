<?php

namespace App\Http\Controllers;

use App\Classes\Parser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ParserController extends Controller
{
    /**
     * Return view main page Parser.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('parser.index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function pars()
    {
        set_time_limit(1000);

        $parser = new Parser(request()->all());
        $models = $parser->products()->models();
        if($models) {
            DB::table('products')->insert($models);
        }
        $result = count($models);
        return response()->json(['result' => $result, 'duplicates' => $parser->duplicates]);
    }
}
