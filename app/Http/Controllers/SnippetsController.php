<?php

namespace App\Http\Controllers;

use App\Models\Snippet;
use Illuminate\Http\Request;

class SnippetsController extends Controller
{
    /**
     * Получение списка сниппетов.
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return Snippet::all();
    }

    /**
     * Обновление данных конкретного сниппета.
     */
    public function update()
    {
        Snippet::find(request()->id)->update([
            'name' => request()->name,
            'template' => request()->template,
            'position' => request()->position,
        ]);
    }

    /**
     * Добавление в базу нового сниппета.
     * @return mixed
     */
    public function store()
    {
        return Snippet::create([
            'name' => request()->name,
            'template' => request()->template,
            'position' => request()->position,
        ]);
    }

    /**
     * Удаление конкретного сниппета.
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function destroy($id)
    {
        Snippet::find($id)->delete();

        return $this->all();
    }
}
