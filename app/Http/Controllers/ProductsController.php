<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Resources\Product as ProductResource;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Отображение страницы редактирования товара.
     * @return ProductResource
     */
    public function edit()
    {
        return new ProductResource(Product::find(request()->id));
    }

    /**
     * Получение списка всех товаров.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function all()
    {
        return ProductResource::collection(Product::orderBy('id', 'desc')->get());
    }

    /**
     * Получение списка товаров, которые находятся в корзине.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function trashed()
    {
        return ProductResource::collection(Product::onlyTrashed()->get());
    }

    /**
     * Полное удаление товара из базы данных.
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function force($id)
    {
        Product::onlyTrashed()->where('id', $id)->forceDelete();

        return $this->trashed();
    }

    /**
     * Восстановление конкретного товара из корзины.
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function recover($id)
    {
        Product::onlyTrashed()->where('id', $id)->restore();

        return $this->trashed();
    }

    /**
     * Подсчет количества товаров в прайсе. Для главной статистики.
     * @return int
     */
    public function count()
    {
        return Product::all()->count();
    }

    /**
     * Подсчет количества товаров, которые находятся в корзине.
     * Для общей статистики.
     * @return mixed
     */
    public function countTrashed()
    {
        return Product::onlyTrashed()->count();
    }

    /**
     * Обновления данных конкретного товара в прайсе.
     */
    public function update()
    {
        Product::find(request('id'))->update([
            'title' => request('title'),
            'code' => request('code'),
            'category_code' => request('category_code'),
            'price' => request('price'),
            'price_o' => request('price_o'),
            'description' => request('description'),
            'verified' => request('verified'),
        ]);
    }

    /**
     * Удаление конкретного товара в корзину.
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function destroy($id)
    {
        Product::find($id)->delete();

        return $this->all();
    }
}
