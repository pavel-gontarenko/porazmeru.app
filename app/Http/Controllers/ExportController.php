<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Snippet;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Resources\Product as ProductResource;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    /**
     * Получение всех товаров, прошедших модерацию.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function table()
    {
        return ProductResource::collection(Product::orderBy('id', 'desc')->where('verified', true)->get());
    }

    /**
     * Формирование файла excel с последующим сохранением в store.
     */
    public function excel()
    {
        $products = $this->adapt(request()->products);
        $data = $this->add($products);

        Excel::create('export_products_from_' . Carbon::now()->format('dmY_His'), function ($excel) use ($data) {
            $excel->sheet('Товары', function ($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->store('xlsx')->download('xlsx');
    }

    /**
     * Создание адаптированного массива товаров под шаблон prom.ua.
     * @param $products
     * @return array
     */
    protected function adapt($products): array
    {
        $productsAdapted = [];

        foreach ($products as $product) {
            $productsAdapted[] = [
                'code' => $product['code'] ?? config('store.export_default')['code'], // Код_товара
                'title' => $product['title'] ?? config('store.export_default')['title'], // Название_позиции
                'tags' => $this->tags($product['category_code']) ?? config('store.export_default')['tags'], // Ключевые_слова
                'description' => $this->description($product['description']) ?? config('store.export_default')['description'], // Описание
                'product_type' => config('store.export_default')['product_type'], // Тип_товара
                'price' => $product['price'] ?? config('export_default')['price'], // Цена
                'currency' => config('store.export_default')['currency'], // Валюта
                'discount' => config('store.export_default')['discount'], // Скидка
                'unit' => config('store.export_default')['unit'], // Единица_измерения
                'min_order' => config('store.export_default')['min_order'], // Минимальный_объем_заказа
                'price_o' => $product['price_o'] ?? config('store.export_default')['price_o'], // Оптовая_цена
                'min_order_o' => config('store.export_default')['min_order_o'], // Минимальный_заказ_опт
                'images' => $this->images($product['images']) ?? config('store.export_default')['images'], // Ссылка_изображения
                'availability' => config('store.export_default')['availability'], // Наличие
                'manufacturer' => config('store.export_default')['manufacturer'], // Производитель
                'country_manufacturer' => config('store.export_default')['country_manufacturer'], // Страна_производитель
                'category' => $product['category_code'] ?? config('store.export_default')['category'], // Номер_группы
                'sub_category' => config('store.export_default')['sub_category'], // Адрес_подраздела
                'possibility_delivery' => config('store.export_default')['possibility_delivery'], // Возможность_поставки
                'delivery_time' => config('store.export_default')['delivery_time'], // Срок_поставки
                'packing_method' => config('store.export_default')['packing_method'], // Способ_упаковки
                'product_id' => md5($product['id']) ?? config('store.export_default')['product_id'], // Идентификатор_товара
                'unique_id' => config('store.export_default')['unique_id'], // Уникальный_идентификатор
                'category_id' => config('store.export_default')['category_id'], // Идентификатор_подраздела
                'group_id' => config('store.export_default')['group_id'], // Идентификатор_группы
                'groups_varieties' => config('store.export_default')['groups_varieties'], // ID_группы_разновидностей
                'characteristic_name_1' => config('store.export_default')['characteristic_name_1'], // Название_Характеристики
                'characteristic_unit_1' => config('store.export_default')['characteristic_unit_1'], // Измерение_Характеристики
                'characteristic_value_1' => config('store.export_default')['characteristic_value_1'], // Значение_Характеристики
                'characteristic_name_2' => config('store.export_default')['characteristic_name_2'], // Название_Характеристики
                'characteristic_unit_2' => config('store.export_default')['characteristic_unit_2'], // Измерение_Характеристики
                'characteristic_value_2' => config('store.export_default')['characteristic_value_2'], // Значение_Характеристики
            ];
        }

        return $productsAdapted;
    }

    /**
     * Добавление шапки к таблице товаров. Объединяет шапку с товарами.
     * Генерируя готовый массив для записи в excel.
     * @param $products
     * @return array
     */
    protected function add($products): array
    {
        $data = [];
        $data[] = config('store.export_template');
        foreach ($products as $product) {
            $data[] = $product;
        }

        return $data;
    }

    /**
     * Добавляет к описанию товара сниппеты header & footer.
     * @param $description
     * @return string
     */
    public function description($description)
    {
        $snippets = Snippet::where('position', '<>', 'off')->get();

        $header = '';
        $footer = '';

        foreach ($snippets as $snippet) {
            if ($snippet->position === 'header') {
                $header .= $snippet->template;
            } else {
                $footer .= $snippet->template;
            }
        }

        return $header . $description . $footer;
    }

    /**
     * Подготавливает данные по изображением товара.
     * @param $images
     * @return string
     */
    protected function images($images): string
    {
        return implode(',', json_decode($images));
    }

    /**
     * Получает теги в зависимости от указанной категории товара.
     * @param $category_code
     * @return null|string
     */
    protected function tags($category_code)
    {
        $list = Tag::where('category_code', $category_code)->first();

        return ($list) ? implode(',', json_decode($list->list)) : null;
    }
}
