<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name' => 'required|max:255|unique:categories,name,'.$request->id,
            'code' => 'required|numeric|digits:8|unique:categories,code,'.$request->id
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Вы не указали название категории',
            'name.unique'   => 'Категория с таким названием уже существует',
            'name.max'      => 'Название категории привысило длину в :max символов',
            'code.required' => 'Вы не указали код категории',
            'code.numeric'  => 'Код категории должен быть числовым',     
            'code.digits'   => 'Код категории должен состоять из 8 цифр',    
            'code.unique'   => 'Категория с таким кодом уже существует',
        ];
    }
}
