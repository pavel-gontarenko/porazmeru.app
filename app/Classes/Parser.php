<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 16.11.2017
 * Time: 16:16
 */

namespace App\Classes;

use App\Models\Product;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Sunra\PhpSimple\HtmlDomParser;

class Parser
{
    /**
     * Подключение к ресурсу.
     * @var Client
     */
    private $client;

    /**
     * Первая часть url - адрес сайта, раздел.
     * @var string
     */
    private $url = 'https://kostumerodessa.com/g2508517-goryachie-novinki/page_';

    /**
     * Вторая часть url - get параметры.
     * @var string
     */
    private $parameters = '?sort=-date_created&product_items_per_page=';

    /**
     * Настройки парсера.
     * Устанавливаются в админке во время парсинга.
     * @var
     */
    private $s;

    /**
     * Коллекция dom страниц.
     * @var
     */
    private $pages;

    /**
     * Коллекция dom товаров.
     * @var
     */
    public $products;

    /**
     * Коллекция моделей товаров.
     * @var
     */
    public $models;

    public $duplicates;

    /**
     * Parser constructor.
     * @param $s
     */
    public function __construct($s)
    {
        $this->client = new Client();
        $this->s = $s;

        for ($i = $s['start']; $i <= $s['end']; $i++) {
            $link = $this->url.$i.$this->parameters.$s['products'];
            $this->pages[] = HtmlDomParser::str_get_html($this->connect($link));
        }
    }

    /**
     * Подключение к необходимой странице.
     * @param $url
     * @return string
     */
    public function connect($url)
    {
        return $this->client->request('GET', $url)
            ->getBody()->getContents();
    }

    /**
     * Получние DOM всех товаров и ссылок на страницы товаров.
     * @return $this
     */
    public function products()
    {
        foreach ($this->pages as $page) {
            $links = $page->find('.b-product-line__product-name-link');

            foreach ($links as $link) {
                $this->products[] = [
                    'dom' => HtmlDomParser::str_get_html($this->connect($link->href)),
                    'link' => $link->href
                ];
            }
        }

        return $this;
    }

    /**
     * Получение заполненных моделей всех товаров.
     * @return mixed
     */
    public function models()
    {
        $num = $this->code();

        foreach ($this->products as $product) {
            $dom = $product['dom'];
            $model = [];

            // Данные с парсинга
            $model['title_k'] = $this->title($dom);
            $model['code_k'] = $this->codeK($dom);

            if ($this->hasProduct($model['title_k'])) {
                $this->duplicates[] = [
                    'code' => $model['code_k'],
                    'link' => $product['link']
                ];
                continue;
            }

            $model['price_k'] = $this->priceK($dom);
            $model['description'] = $this->description($dom);
            $model['link'] = $product['link'];

            // Расчетные данные
            $model['created_at'] = Carbon::now();
            $model['code'] = config('store.prefix') . $num;
            $model['images'] = $this->images($dom);
            if ($this->s['price']) {
                list($model['price'], $model['price_o']) = $this->price($model['price_k']);
            }
            if($this->s['photos']) {
                $this->getPhotos($model['images'], $model['code']);
            }

            $this->models[] = $model;
            $num++;
        }

        return $this->models;
    }

    /**
     * Проверяет наличие дубликатов товаров в базе.
     * @param string $title
     * @return bool
     */
    private function hasProduct(string $title): bool
    {
        return Product::withTrashed()->where('title_k', $title)->exists();
    }

    /**
     * Получение номера кода товара для добавления записи в базу.
     * @return int
     */
    private function code(): int
    {
        $lastProduct = Product::withTrashed()->where('code', 'like', 'sp%')->orderBy('id', 'desc')->first();

        return $lastProduct ? substr($lastProduct->code, 2) + 1 : 1;
    }


    /**
     * Получение из DOM названия товара.
     * @param $dom
     * @return mixed
     */
    private function title($dom): string
    {
        return $dom->find('h1.b-title_type_b-product', 0)->plaintext;
    }

    /**
     * Получение из DOM кода товара у поставщика.
     * @param $dom
     * @return string
     */
    private function codeK($dom)
    {
        $value = $dom->find('span.b-product__sku', 0)->plaintext ?? null;
        // Обрезается строка "Код: " и пробелы по краям.
        // Необходимость обрезки пробелов под вопросом.
        return $value ? trim(mb_substr($value, 5, null)) : '';
    }

    /**
     * Получение из DOM цены товара у поставщика.
     * @param $dom
     * @return int
     */
    private function priceK($dom)
    {
       // return (int)$dom->find('span[itemprop=price]', 0)->getAttribute('content');
       return (int)preg_replace("/[^0-9]/", '', $dom->find('.b-product__price', 0)->text());
    }

    /**
     * Получение пересчитаной цены товара для маназина + оптовая цена (USD)
     * @param int $priceK
     * @return array
     */
    private function price(int $priceK): array
    {
        $prices[] = round((config('store.extra_charge') + $priceK) / config('store.dollar_rate'), 2);
        $prices[] = round($prices[0] - config('store.discount'), 2);

        return $prices;
    }

    /**
     * Получение из DOM описания товара.
     * @param $dom
     * @return mixed
     */
    private function description($dom)
    {
        return ($this->s['format'])
            ? $dom->find('.b-user-content', 0)->innertext() // с форматированием
            : $dom->find('.b-user-content', 0)->text(); // без
    }

    /**
     * Получение из DOM изображений товара в виде массива.
     * @param $dom
     * @return array
     */
    private function images($dom)
    {
        // Массив с фотографиями.
        $images = [];
        // На дальнейшее, можно регулировать какая фотография будет отображаться главной
        $images[0] = $dom->find('a[class=js-product-gallery-overlay b-centered-image b-product__image]', 0)->href;

        // Добавляем остальные фото в массив
        $rest = $dom->find('div.b-product__additional-holder a');
        foreach ($rest as $image) {
            $images[] = $image->href;
        }

        return json_encode($images);
    }

    private function getPhotos($images, $code)
    {
        $i = 1;
        foreach (json_decode($images) as $image) {
            $contents = file_get_contents($image);
            Storage::put('photos/'.$code.'/'.$code.'_'.$i.'.jpg', $contents);
            $i++;
        }
    }
}