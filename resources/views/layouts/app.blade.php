<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--CSS styles--}}
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <title>Dashboard</title>
</head>
<body>
<div id="app">
    <my-header></my-header>
    <div class="columns">
        <sidebar></sidebar>
        <div class="column">
            <router-view></router-view>
        </div>
    </div>
    <my-footer></my-footer>
</div>


{{--JS Scripts--}}
<script>
    window.Laravel = '{{ json_encode(['csrfToken' => csrf_token()]) }}'
</script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>