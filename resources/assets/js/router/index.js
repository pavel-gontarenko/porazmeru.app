import Vue from 'vue';
import Router from 'vue-router';

// Components Dashboard
import DashboardIndex from '../components/dashboard/index.vue';

// Components Products
import ProductsIndex from '../components/products/index.vue';
import ProductEdit from '../components/products/edit.vue';
import ProductsTrashed from '../components/basket/index.vue';

// Components Parser
import ParserIndex from '../components/parser/index.vue';

// Components Categories
import CategoriesIndex from '../components/categories/index.vue';

// Components Tags
import TagsIndex from '../components/tags/index.vue';
import AddTagsIndex from '../components/tags/add.vue';

// Snippets Tags
import SnippetsIndex from '../components/snippets/index.vue';
import AddSnippetsIndex from '../components/snippets/add.vue';

// Components Export
import ExportIndex from '../components/export/index.vue';

// Components Import
import ImportIndex from '../components/import/index.vue';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'DashboardIndex',
            component: DashboardIndex
        },
        {
            path: '/products',
            name: 'ProductsIndex',
            component: ProductsIndex
        },
        {
            path: '/products/:id',
            name: 'ProductEdit',
            component: ProductEdit
        },
        {
            path: '/parser',
            name: 'ParserIndex',
            component: ParserIndex
        },
        {
            path: '/categories',
            name: 'CategoriesIndex',
            component: CategoriesIndex
        },
        {
            path: '/tags',
            name: 'TagsIndex',
            component: TagsIndex
        },
        {
            path: '/tags/add',
            name: 'AddTagsIndex',
            component: AddTagsIndex
        },
        {
            path: '/snippets',
            name: 'SnippetsIndex',
            component: SnippetsIndex
        },
        {
            path: '/snippets/add',
            name: 'AddSnippetsIndex',
            component: AddSnippetsIndex
        },
        {
            path: '/export',
            name: 'ExportIndex',
            component: ExportIndex
        },
        {
            path: '/import',
            name: 'ImportIndex',
            component: ImportIndex
        },
        {
            path: '/basket',
            name: 'ProductsTrashed',
            component: ProductsTrashed
        },
    ],
   // mode: 'history'
})