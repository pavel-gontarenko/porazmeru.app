
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue'
import router from './router';
import Buefy from 'buefy';
import 'buefy/lib/buefy.css';
import VueCarousel from 'vue-carousel';



import VueQuillEditor from 'vue-quill-editor';

// require styles
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';

Vue.use(VueQuillEditor, /* { default global options } */);

Vue.use(VueCarousel);
Vue.use(Buefy);

Vue.component('example-component', require('./components/ExampleComponent.vue'));

// Components Sections
let MyHeader = require('./components/sections/MyHeader.vue');
let MyFooter = require('./components/sections/MyFooter.vue');
let Sidebar = require('./components/sections/Sidebar.vue');

const app = new Vue({
    el: '#app',
    router,
    components: { MyHeader, MyFooter, Sidebar }
});
