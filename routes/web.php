<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

// TODO: переделать где возможно на префиксы.

// Dashboard Main Page
Route::get('/', 'DashboardController@index');

Route::get('/{path}', function () {
    return redirect('/');
})->where( 'path', '(.*)?' );

// Categories
Route::resource('/categories', 'CategoryController');
Route::post('/categories/all', 'CategoryController@all');
Route::post('/categories/count', 'CategoryController@count');

// Tags
Route::post('/tags/all', 'TagsController@all');
Route::post('/tags', 'TagsController@update');
Route::delete('/tags/{id}', 'TagsController@destroy');
Route::post('/tags/store', 'TagsController@store');

// Snippets
Route::post('/snippets/all', 'SnippetsController@all');
Route::post('/snippets', 'SnippetsController@update');
Route::delete('/snippets/{id}', 'SnippetsController@destroy');
Route::post('/snippets/store', 'SnippetsController@store');

// Products
Route::post('/products/all', 'ProductsController@all');
Route::post('/products/trashed', 'ProductsController@trashed');
Route::post('/products/edit', 'ProductsController@edit');
Route::post('/products/update', 'ProductsController@update');
Route::post('/products/count', 'ProductsController@count');
Route::post('/products/count-trashed', 'ProductsController@countTrashed');
Route::delete('/products/{id}', 'ProductsController@destroy');
Route::delete('/products/trashed/{id}', 'ProductsController@recover');
Route::delete('/products/force/{id}', 'ProductsController@force');

// Parser
Route::post('/parser', 'ParserController@pars');

// Export
Route::post('/export/table', 'ExportController@table');
Route::post('/export/excel', 'ExportController@excel');


