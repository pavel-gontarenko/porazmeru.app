<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->comment('Название товара');
            $table->string('title_k')->comment('Название товара у поставщика');   
            $table->string('code')->nullable()->comment('Код товара');            
            $table->string('code_k')->comment('Код товара у поставщика');
            $table->decimal('price', 6, 2)->nullable()->comment('Цена товара, валюта $');  
            $table->decimal('price_o', 6, 2)->nullable()->comment('Оптовая цена товара, валюта $');              
            $table->decimal('price_k', 6, 2)->comment('Цена товара у поставщика, валюта грн.');
            $table->text('description')->comment('Описание товара');
            $table->jsonb('images')->comment('Список url картинок');
            $table->string('link')->comment('Ссылка на страницу с товаром');

            $table->boolean('verified')->default(false)->comment('Модерация товара');
            $table->unsignedInteger('category_code')->comment('Связь товара с категорией')->nullable();
            $table->unsignedInteger('site_id')->comment('Связь товара с поставщиком')->nullable();
            $table->unsignedInteger('prom_category_id')->comment('Связь товара с категорией на prom.ua')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
